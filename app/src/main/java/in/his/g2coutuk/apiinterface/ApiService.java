package in.his.g2coutuk.apiinterface;


import in.his.g2coutuk.dataModel.BayResponse;
import in.his.g2coutuk.dataModel.CommonList;
import in.his.g2coutuk.dataModel.GetOrderDetail;
import in.his.g2coutuk.dataModel.GetTransportData;
import in.his.g2coutuk.dataModel.InsertManualProduction;
import in.his.g2coutuk.dataModel.InsertModel;
import in.his.g2coutuk.dataModel.InsertOutGoods;
import in.his.g2coutuk.dataModel.LocationChangeData;
import in.his.g2coutuk.dataModel.LocationChangeResponse;
import in.his.g2coutuk.dataModel.ProductionAllResponse;
import in.his.g2coutuk.dataModel.ResponseAllBayDetails;
import in.his.g2coutuk.dataModel.ResponseAllData;
import in.his.g2coutuk.dataModel.ResponseAllSku;
import in.his.g2coutuk.dataModel.ResponseInsertProduction;
import in.his.g2coutuk.dataModel.SkuItem;
import in.his.g2coutuk.dataModel.SkuListResponse;
import in.his.g2coutuk.dataModel.Password;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    /*http://192.168.43.106:8082/login*/

    @GET("getLogin/")
    Call<Password> PASSWORD_CALL(@Query("user_name") String user_name,
                                 @Query("password") String password);

    @GET("getSkuListData/")
    Call<SkuListResponse> SKU_LIST_RESPONSE_CALL();

    @GET("getAllProductionData/")
    Call<ProductionAllResponse> PRODUCTION_ALL_RESPONSE_CALL();

    @GET("getBayList/")
    Call<BayResponse> BAY_RESPONSE_CALL();

    @GET("getCommonData/")
    Call<CommonList> COMMON_LIST_CALL();

    @POST("updateCommonDataStatus")
    Call<LocationChangeResponse> UPDATE_COMMON_DATA_STATUS(@Query("id") String id,
                                                           @Query("status") String status);
    /*
    response msg Updated Successfully
    */

    @GET("getLocationChange/")
    Call<LocationChangeData> LOCATION_CHANGE_DATA_CALL(@Query("id") String id);

    @GET("getTransportStatus/")
    Call<GetTransportData> GET_TRANSPORT_DATA_CALL(@Query("order_id") String order_id);

    @GET("getOrderProduct/")
    Call<GetOrderDetail> GET_ORDER_DETAIL_CALL(@Query("order_id") String order_id);

    @POST("insertOutGoods")
    Call<LocationChangeResponse> INSERT_OUT_GOODS_CALL(@Body InsertOutGoods insertOutGoods, @Query("cQty") int cQty);

    @POST("updateProductionOrder")
    Call<LocationChangeResponse> INSERT_GOODS_CALL(@Query("order_id") String order_id ,@Body InsertModel insertGoods);

    @GET("getBayList/")
    Call<ResponseAllBayDetails> BAY_RESPONSE_BARCODE_CALL();

    @GET("getSkuListData/")
    Call<ResponseAllSku> SKU_LIST_BARCODE_RESPONSE_CALL();

    @GET("getBayWithProduction/")
    Call<ResponseAllData> PRODUCTION_ALL_BARCODE_RESPONSE_CALL();

    @POST("updateManualProductionOrder/")
    Call<ResponseInsertProduction> INSERT_MANUAL_PRODUCTION(@Body InsertManualProduction insertManualProduction);

    @POST("updateSku/")
    Call<Password> SKU_ITEM_CALL(@Body SkuItem skuItem);
}
