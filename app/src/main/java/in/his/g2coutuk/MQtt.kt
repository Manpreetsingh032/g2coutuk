package `in`.his.g2coutuk

import android.content.Context
import android.util.Log
import android.widget.Toast
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*

class MQtt {
    private var ip = "18.217.174.27"
    private var port = "1883"
    private var id = ""
    private lateinit var context: Context
    private var connectionFailStatus = false
    private lateinit var token: IMqttToken
    var disconnect = true

    fun mQTTConnect(context: Context, id: String) {
        this.context = context
        this.id = MqttClient.generateClientId()
        client = MqttAndroidClient(context.applicationContext, "tcp://$ip:$port", this.id)
        val options = MqttConnectOptions()
        /*      options.userName = "pernod"
              options.password = "pernod1234".toCharArray()*/
        options.isCleanSession = true
        //Making it will receive all those messages which were published when you were online.
        options.isAutomaticReconnect = false
        token = client!!.connect(options)
        try {
            Log.d("TAG", "IN TRY")
            token.actionCallback = object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    confirmConnection.onSuccess()
                    receiveMsgCallback()
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    if (!connectionFailStatus) {
                        Toast.makeText(
                            context,
                            exception.toString(),
                            Toast.LENGTH_LONG
                        ).show()
                        connectionFailStatus = true
                    }
                    if (disconnect) {
                        disConn()
                        confirmConnection.onUnSuccess()
                        Log.d("TAG", "M QTT Connection Lost Reason: $exception")
                    }
                    disconnect = true
                }
            }
        } catch (e: Exception) {
            Log.d("TAG", "Exception Mqtt $e")
        }
        /*     token = client?.connect(options, context, object : IMqttActionListener {
                 override fun onSuccess(asyncActionToken: IMqttToken?) {


                 }

                 override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {

                 }
             })!!*/
    }

    fun subscribeTopic(c: Context, name: String) {
        try {
            token.client!!.subscribe(name, 0)
            Log.d("Subs $name ", name)
        } catch (e: Exception) {
            runCatching {
                Toast.makeText(c, "Error Subscribing Topic $name", Toast.LENGTH_SHORT).show()
            }
            Log.d("Subs $name ", e.toString())
        }
    }

    private fun receiveMsgCallback() {
        token.client?.setCallback(object : MqttCallback {
            override fun connectionLost(cause: Throwable?) {
                Log.d("TAG", "Connection Lost Reason: $cause")
                confirmConnection.onUnSuccess()
                disconnect = false
            }

            @Throws(Exception::class)
            override fun messageArrived(topic: String, message: MqttMessage) {
                val common = String(message.payload)
                Log.d("TAG COMMON", common)
                try {
                    when (topic) {
                        "location" -> {
                            mqttValues.onLocation(String(message.payload))
                        }
                        "status" -> {
                            mqttValues.onStatus(String(message.payload))
                        }
                        else -> {
                            mqttValues.onOthers(topic, String(message.payload))
                        }
                    }
                } catch (e: Exception) {
                    Log.d("TAG", "messageArrived: $topic" + e.message)
                }
            }

            override fun deliveryComplete(token: IMqttDeliveryToken) {
                Log.d("TAG", "Delivery Complete")
            }
        })
    }

    fun publish(topic: String?, msg: ByteArray) {
        try {
            token.client?.publish(topic, msg, 1, false)
            Log.d("Publish", "$topic, $msg")
        } catch (e: Exception) {
            Log.d("Pub", "$topic ,$e")
        }
    }

    private fun disConn() {
        try {
            token.client.disconnect(context.applicationContext, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.d("TAG", "Disconnect")

                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.d("TAG", "Failed to Disconnect")
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private var client: MqttAndroidClient? = null
        private var instance: MQtt? = null
        private lateinit var confirmConnection: ConfirmConnection
        private lateinit var mqttValues: MqttValues

        val managerInstance: MQtt
            get() {
                if (instance == null) {
                    instance = MQtt()
                }
                return instance as MQtt
            }
    }

    fun addConnectionCalls(connection: ConfirmConnection) {
        confirmConnection = connection
    }

    fun setValueListener(values: MqttValues) {
        mqttValues = values
    }
}

interface ConfirmConnection {
    fun onSuccess()
    fun onUnSuccess()
}

interface MqttValues {
    fun onStatus(status: String)
    fun onLocation(location: String)
    fun onOthers(topic: String, string: String)
}