package `in`.his.g2coutuk

interface HandleClick {
    fun  clickBay(bay : String)
}

class ClickObjHandle{
    lateinit var handleClick: HandleClick
    fun addClick(handleClick: HandleClick){
        this.handleClick = handleClick
    }
    companion object{
        private lateinit var clickObjHandle: ClickObjHandle
        fun instance(): ClickObjHandle {
            return if (this::clickObjHandle.isInitialized){
                clickObjHandle
            }
            else{
                clickObjHandle = ClickObjHandle()
                clickObjHandle
            }
        }
    }
}
