package in.his.g2coutuk.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import in.his.g2coutuk.MainActivity;
import in.his.g2coutuk.Utils;
import in.his.g2coutuk.LoginActivity;
import in.his.g2coutuk.R;


public class Splash extends AppCompatActivity {
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Utils().getAllData();

        handler.postDelayed(() -> {
            startActivity(new Intent(Splash.this, LoginActivity.class));
            finish();
        }, 1500);
    }
}
