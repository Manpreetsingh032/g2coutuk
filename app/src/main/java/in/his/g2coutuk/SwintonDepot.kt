package `in`.his.g2coutuk

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.uk1.*

class SwintonDepot : Fragment() {
    private lateinit var handleClick: HandleClick

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.uk1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.handleClick = ClickObjHandle.instance().handleClick

        ViewListener.instance().addBayView(object : SetBayView {
            override fun changeView() {
                findEmptyBay()
            }
        })

        tdg_a3?.setOnClickListener {
            performClick("TDG A3")

        }
        tdg_b3?.setOnClickListener {
            performClick("TDG B3")

        }
        tdg_c3?.setOnClickListener {
            performClick("TDG C3")

        }
        tdg_a4?.setOnClickListener {
            performClick("TDG A4")

        }
        tdg_b4?.setOnClickListener {
            performClick("TDG B4")

        }
        tdg_c4?.setOnClickListener {
            performClick("TDG C4")

        }
        tdg_d1?.setOnClickListener {
            performClick("TDG D1")

        }
        tdg_c1?.setOnClickListener {
            performClick("TDG C1")

        }
        tdg_b1?.setOnClickListener {
            performClick("TDG B1")

        }
        tdg_a1?.setOnClickListener {
            performClick("TDG A1")

        }
        tdg_d2?.setOnClickListener {
            performClick("TDG D2")

        }
        tdg_c2?.setOnClickListener {
            performClick("TDG C2")

        }
        tdg_b2?.setOnClickListener {
            performClick("TDG B2")

        }
        tdg_a2?.setOnClickListener {
            performClick("TDG A2")

        }
        tdg_a5?.setOnClickListener {
            performClick("TDG A5")

        }
        tdg_b5?.setOnClickListener {
            performClick("TDG B5")

        }
        tdg_c5?.setOnClickListener {
            performClick("TDG C5")

        }
        tdg_a6?.setOnClickListener {
            performClick("TDG A6")

        }
        tdg_b6?.setOnClickListener {
            performClick("TDG B6")

        }
        tdg_c6?.setOnClickListener {
            performClick("TDG C6")

        }
        zone_5a?.setOnClickListener {
            performClick("CUSTOM INSP AREA ZONE 5A")

        }
        zone_5b5c?.setOnClickListener {
            performClick("DISPATCH AREA ZONE 5B+5C")

        }
        zone_6f6g?.setOnClickListener {
            performClick("DOCUMENT + POSTAL AREA ZONE 6F+6G")

        }
        zone_5c5e5d?.setOnClickListener {
            performClick("PACKING + REPACKING STATION DESK ZONE 5C+5E+5D")

        }
        left_path?.setOnClickListener {
            performClick("LEFT-PATH")

        }
        right_path?.setOnClickListener {
            performClick("RIGHT-PATH")

        }
        top_path?.setOnClickListener {
            performClick("TOP-PATH")

        }
        bottom_left_path?.setOnClickListener {
            performClick("BOTTOM-LEFT-PATH")

        }
        bottom_right_path?.setOnClickListener {
            performClick("TDG B34-PATH")

        }
        z5a_path?.setOnClickListener {
            performClick("Z5A-PATH")

        }
        c3_path?.setOnClickListener {
            performClick("C3-PATH")

        }
        z5b5c_path?.setOnClickListener {
            performClick("Z5B5C-PATH")

        }
        d1_path?.setOnClickListener {
            performClick("D1-PATH")

        }
        a6_path?.setOnClickListener {
            performClick("A6-PATH")

        }
        d1d2_path?.setOnClickListener {
            performClick("D1D2-PATH")

        }
        d2a5_path?.setOnClickListener {
            performClick("D2A5-PATH")

        }
        c5_path?.setOnClickListener {
            performClick("C5-PATH")

        }
        z6f6g_path?.setOnClickListener {
            performClick("Z6F6G-PATH")

        }
        z5c5d5e_path?.setOnClickListener {
            performClick("Z5C5D5E-PATH")

        }
        z5c5f_path?.setOnClickListener {
            performClick("Z5C5F-PATH")

        }
        z5f_path?.setOnClickListener {
            performClick("Z5F-PATH")
        }

        findEmptyBay()
    }

    private fun performClick(name: String) {
        if (this::handleClick.isInitialized) {
            handleClick.clickBay(name)
        }
    }

    private fun findEmptyBay() {

        setBack("TDG A3", tdg_a3)

        setBack("TDG B3", tdg_b3)

        setBack("TDG C3", tdg_c3)

        setBack("TDG A4", tdg_a4)

        setBack("TDG B4", tdg_b4)

        setBack("TDG C4", tdg_c4)

        setBack("TDG D1", tdg_d1)

        setBack("TDG C1", tdg_c1)

        setBack("TDG B1", tdg_b1)

        setBack("TDG A1", tdg_a1)

        setBack("TDG D2", tdg_d2)

        setBack("TDG C2", tdg_c2)

        setBack("TDG B2", tdg_b2)

        setBack("TDG A2", tdg_a2)

        setBack("TDG A5", tdg_a5)

        setBack("TDG A6", tdg_a6)

        setBack("TDG B5", tdg_b5)

        setBack("TDG C5", tdg_c5)

        setBack("TDG B6", tdg_b6)

        setBack("TDG B6", zone_5a)

        setBack("TDG B6", zone_5b5c)

        setBack("TDG B6", zone_6f6g)

        setBack("TDG B6", zone_5c5e5d)

    }

    private fun setBack(name: String, layout: LinearLayout?) {
        try {
            val list = Search().searchFromBay(name)
            if (list[0].sku.equals("empty", true)) {
                layout?.background =
                    ResourcesCompat.getDrawable(resources, R.drawable.empty_back, null)
            } else {
                layout?.background =
                    ResourcesCompat.getDrawable(resources, R.drawable.main_back, null)
            }
        } catch (e: Exception) {
            Log.d("TAG", "setBack: $e")
        }
    }


    companion object {
        fun newInstance() =
            SwintonDepot()
    }
}