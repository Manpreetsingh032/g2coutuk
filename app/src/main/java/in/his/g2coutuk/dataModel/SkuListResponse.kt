package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class SkuListResponse(

    @field:SerializedName("SkuData")
    val skuData: List<SkuDataItem?>? = null
)

/*
"SkuData": [
        {
            "id": 1,
            "sku": "BPWQ011CHDCVL",
            "cases_of_pallets": 40.0
        }
*/

data class SkuDataItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("sku")
    val sku: String? = null,

    @field:SerializedName("cases_of_pallets")
    val casesOfPallets: Double? = null
)
