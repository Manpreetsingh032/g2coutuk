package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class ProductionPlanResponse(

    @field:SerializedName("proplan")
    val proplan: List<ProplanItem?>? = null
)

data class ProplanItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("batch_no")
    val batchNo: String? = null,

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("line_no")
    val lineNo: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("sku")
    val sku: String? = null
)
