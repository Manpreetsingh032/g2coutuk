package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseAllBayDetails(

	@field:SerializedName("bay")
	val bayDetails: List<BayItemDetails?>? = null
)

data class BayItemDetails(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("bay")
	val bay: String? = null,

	@field:SerializedName("barcode")
	val barcode: Int? = null,

	@field:SerializedName("capacity")
	val capacity: Double? = null
)
