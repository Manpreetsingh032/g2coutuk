package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class GetTransportData(
    @field:SerializedName("TransportStatusData")
    val transportStatusData: List<TransportStatusDataItem?>? = null
)

data class TransportStatusDataItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("vehicle_no")
    val vehicleNo: String? = null,

    @field:SerializedName("permit_no")
    val permitNo: String? = null,

    @field:SerializedName("total_weight")
    val totalWeight: String? = null,

    @field:SerializedName("driver_name")
    val driverName: String? = null,

    @field:SerializedName("truck_bay_no")
    val truckBayNo: String? = null,

    @field:SerializedName("party_name")
    val partyName: String? = null,

    @field:SerializedName("total_qty")
    val total_qty: String? = null,

    @field:SerializedName("contact_no")
    val contactNo: String? = null,

    @field:SerializedName("state")
    val state: String? = null,

    @field:SerializedName("order_id")
    val orderId: Int? = null,

    @field:SerializedName("status")
    val status: Int? = null
)
