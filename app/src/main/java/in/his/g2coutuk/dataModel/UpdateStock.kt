package `in`.his.g2cout2.dataModel

import com.google.gson.annotations.SerializedName

data class UpdateStock(

	/*
	{ "barcode":"0046800111",
 "no_of_pcs":40,
 "name_of_item":"polm",
 "per_pcs_weight":"5000.35",
 "packaging":4,
  "carton_gross_weight":"45.36",
 "hsn":"45698712369"
}
	* */

	@field:SerializedName("hsn")
	val hsn: String? = null,

	@field:SerializedName("carton_gross_weight")
	val cartonGrossWeight: String? = null,

	@field:SerializedName("no_of_pcs")
	val noOfPcs: String? = null,

	@field:SerializedName("name_of_item")
	val nameOfItem: String? = null,

	@field:SerializedName("packaging")
	val packaging: String? = null,

	@field:SerializedName("barcode")
	val barcode: String? = null,

	@field:SerializedName("per_pcs_weight")
	val perPcsWeight: String? = null,

	@field:SerializedName("user_name")
	val userName: String? = null
)
