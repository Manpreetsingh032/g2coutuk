package `in`.his.g2cout2.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseProductList(

	@field:SerializedName("productDetails")
	val productDetails: List<ProductDetailsItem?>? = null
)

data class ProductDetailsItem(

	@field:SerializedName("hsn")
	val hsn: Any? = null,

	@field:SerializedName("carton_gross_weight")
	val cartonGrossWeight: Double? = null,

	@field:SerializedName("name_of_item")
	val nameOfItem: String? = null,

	@field:SerializedName("no_of_pcs")
	val noOfPcs: Int? = null,

	@field:SerializedName("packaging")
	val packaging: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("barcode")
	val barcode: String? = null,

	@field:SerializedName("per_pcs_weight")
	val perPcsWeight: Double? = null,

	@field:SerializedName("carton_net_weight")
	val cartonNetWeight: Double? = null
)
