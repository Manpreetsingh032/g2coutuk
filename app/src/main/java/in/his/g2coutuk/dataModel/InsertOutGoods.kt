package `in`.his.g2coutuk.dataModel

class InsertOutGoods(
    var batch_no: String? = null,
    var sku: String? = null,
    var qty: String? = null,
    var order_id: String? = null,
    var expiry: String? = null,
    var barcode: String? = null,
    var p_barcode: String? = null,
) {

    /*
    *  private String batch_no;
    private String barcode;
    private String sku;
    private int qty;
    private long order_id;
    private String expiry;
    private String p_barcode;*/
}