package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class CommonList(

    @field:SerializedName("CommonData")
    val commonData: List<CommonDataItem?>? = null
)

data class CommonDataItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("priority")
    val priority: String? = null,

    @field:SerializedName("order_id")
    val orderId: Int? = null,

    @field:SerializedName("status")
    val status: Int? = null
)
