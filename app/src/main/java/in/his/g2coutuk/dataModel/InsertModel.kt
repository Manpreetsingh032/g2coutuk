package `in`.his.g2coutuk.dataModel

class InsertModel(
    /**
     * sku : BPWQ011CHDCVL
     * batch_no : 2
     * qty : 21
     * bay_no : B1
     * status : Pass
     */
    var sku: String? = null,
    var batch_no: String? = null,
    var qty: String? = null,
    var bay_no: String? = null,
    var status: String? = null
)