package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseBarcodeScan(

    @field:SerializedName("data")
    val data: List<DataItem?>? = null
)

/*
    {
    "data": [
  {
    "id": 2,
    "barcode": "092399613619",
    "name_of_item": "KACHHA AAM JAR b",
    "no_of_pcs": 56,
    "per_pcs_weight": 2.5,
    "packaging": 12,
    "carton_gross_weight": 9.5,
    "hsn": "741852963",
    "qty": 1,
    "status": "valid",
    "date": "2021-04-05"
    }
    ],
    }
 */
data class DataItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("hsn")
    val hsn: String? = null,

    @field:SerializedName("carton_gross_weight")
    val cartonGrossWeight: Double? = null,

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("name_of_item")
    val nameOfItem: String? = null,

    @field:SerializedName("no_of_pcs")
    val noOfPcs: Int? = null,

    @field:SerializedName("packaging")
    val packaging: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("barcode")
    val barcode: String? = null,

    @field:SerializedName("per_pcs_weight")
    val perPcsWeight: Double? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("user_name")
    val userName: String? = null
)
