package `in`.his.g2cout2.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseSalesNos(

	@field:SerializedName("salesNo")
	val salesNo: List<SalesNoItem?>? = null
)

data class SalesNoItem(

	@field:SerializedName("sales_no")
	val salesNo: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
