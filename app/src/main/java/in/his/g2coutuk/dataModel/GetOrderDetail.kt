package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class GetOrderDetail(
    @field:SerializedName("OrderIdProduct")
    val orderIdProduct: List<OrderIdProductItem?>? = null
)

data class OrderIdProductItem(

    /*{"OrderIdProduct":[{"order_id":1,"permit_no":"1112",
    "sku":"testint_as","batch_no":"2021-05-29",
    "barcode":"100000066","expiry":"2021-05-29",
    "p_barcode":"131313",
    "bay":"TDG A3","qty":1}]}*/
    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("batch_no")
    val batchNo: String? = null,

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("bay")
    val bay: String? = null,

    @field:SerializedName("barcode")
    val barcode: String? = null,

    @field:SerializedName("expiry")
    val expiry: String? = null,

    @field:SerializedName("p_barcode")
    val pBarcode: String? = null,

    @field:SerializedName("sku")
    val sku: String? = null,

    @field:SerializedName("order_id")
    val orderId: Int? = null,

    @field:SerializedName("permit_no")
    val permitNo: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)
