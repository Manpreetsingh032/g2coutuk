package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class ProductionAllResponse(

    @field:SerializedName("production")
    val production: List<ProductionItem?>? = null
)

data class ProductionItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("batch_no")
    val batchNo: String? = null,

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("sku")
    val sku: String? = null,

    @field:SerializedName("bay_no")
    val bayNo: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
