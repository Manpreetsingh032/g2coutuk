package `in`.his.g2coutuk.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseAllData(

    @field:SerializedName("bayWithProduction")
    val bayWithProduction: List<BayWithProductionItem?>? = null
)

data class BayWithProductionItem(

    @field:SerializedName("qty")
    val qty: Int? = null,

    @field:SerializedName("expiry")
    val expiry: String? = null,

    @field:SerializedName("bay")
    val bay: String? = null,

    @field:SerializedName("sku")
    val sku: String? = null,

    @field:SerializedName("barcode")
    val barcode: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("p_barcode")
    val pBarcode: Any? = null
)
