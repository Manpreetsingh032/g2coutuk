package in.his.g2coutuk;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import in.his.g2coutuk.apiinterface.ApiService;
import in.his.g2coutuk.apiinterface.RetroClient;
import in.his.g2coutuk.dataModel.Password;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    public static Retrofit retrofit;
    public static ApiService apiservice;
    EditText password, username;
    TextView btn_login;
//    final String userId = "Admin";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_login);
        retrofit = RetroClient.getClient();
        apiservice = retrofit.create(ApiService.class);

        password = findViewById(R.id.login_et_password);
        username = findViewById(R.id.login_et_username);
        btn_login = findViewById(R.id.login_tv_login);
        btn_login.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_tv_login) {
            checkPassword();
        }
    }

//    /*{"userId":"Pernod","password":"12345"}*/

    private void checkPassword() {
        final String pass = password.getText().toString();
        final String userId = username.getText().toString();
        if (!pass.isEmpty() || !userId.isEmpty()) {
            Call<Password> passwordCall = apiservice.PASSWORD_CALL(userId, pass);
            passwordCall.enqueue(new Callback<Password>() {
                @Override
                public void onResponse(Call<Password> call, Response<Password> response) {
                    if (response.body() != null && response.body().getMessage() != null) {
                        if ("Successful".equals(response.body().getMessage())) {
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        } else {
                            password.setError("Wrong Password");
                        }
                    } else {
                        Toast.makeText(LoginActivity.this,
                                "Please Check Network Connection First",
                                Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Password> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.getMessage());
                    Toast.makeText(LoginActivity.this, "Please Check Network Connection First", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(LoginActivity.this,
                    "Please fill fields properly",
                    Toast.LENGTH_SHORT).show();
        }
    }

  /*  private void getPlan() {
        Call<ProductionPlanResponse> call = apiservice.PRODUCTION_PLAN_RESPONSE_CALL();
        call.enqueue(new Callback<ProductionPlanResponse>() {
            @Override
            public void onResponse(Call<ProductionPlanResponse> call, Response<ProductionPlanResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (Objects.requireNonNull(response.body().getProplan()).size() != 0) {
                            Utils.Companion.setList(response.body().getProplan());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductionPlanResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this,
                        "Please Check Network Connection First", Toast.LENGTH_SHORT).show();
            }
        });
    }*/

}
