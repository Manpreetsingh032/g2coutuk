package `in`.his.g2coutuk

import `in`.his.g2coutuk.dataModel.BayWithProductionItem

class Search {
    fun searchFromBay(bayName: String?):ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.bay.equals(bayName, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromAllExceptEmpty(empty: String?): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (!store!!.sku.equals(empty, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromAll(keySearch: String): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.sku!!.contains(keySearch, true)
                    || store.expiry!!.toString().contains(keySearch, true)
                    || store.bay!!.contains(keySearch, true)
                ) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromBlock(block: String): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.bay!!.contains("" + block, true)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun searchFromBatch(batchNo: String): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.expiry.toString().contains(batchNo)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }

    fun getSkuItem(sku: String?): ArrayList<BayWithProductionItem> {
        val searchList = ArrayList<BayWithProductionItem>()
        try {
            for (store in Utils.allData) {
                if (store!!.sku.toString().contentEquals(sku!!)) {
                    searchList.add(store)
                }
            }
        } catch (e: Exception) {
        }
        return searchList
    }
}