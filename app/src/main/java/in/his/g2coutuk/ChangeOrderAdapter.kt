package `in`.his.g2coutuk

import `in`.his.g2coutuk.dataModel.LocationChangeItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.bay_location_change.view.*
import java.util.*

class ChangeOrderAdapter : RecyclerView.Adapter<ChangeOrderAdapter.MyViewHolder>() {
    private var list: ArrayList<LocationChangeItem?> = ArrayList()
    private var status: Boolean = true
    private lateinit var setItemClick: SetItemClick
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.change_number_count.text = (position + 1).toString()
        holder.itemView.item_old_bay_no.text = list[position]?.oldBay
        holder.itemView.item_new_bay_no.text = list[position]?.bayNo
        holder.itemView.item_change_sku.text = list[position]?.sku
        holder.itemView.item_change_batch_no.text = list[position]?.batchNo.toString()
        holder.itemView.item_change_qty.text = list[position]?.qty.toString()

        if (!status) {
            setItemClick.onclick(
                list[position]?.oldBay,
                list[position]?.bayNo,
                list[position]?.sku,
                list[position]?.batchNo.toString(),
                list[position]?.qty.toString()
            )
        }

        if (status) {
            holder.itemView.setBackgroundResource(R.color.yellow)
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.bay_location_change, parent, false)
        return MyViewHolder(v)
    }

    fun setList(list: ArrayList<LocationChangeItem?>, status: Boolean) {
        this.list = list
        this.status = status
        notifyDataSetChanged()
    }

    fun addItemClick(setItemClick: SetItemClick) {
        this.setItemClick = setItemClick
    }

    /**
     * View holder class
     */
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

}

interface SetItemClick {
    fun onclick(
        oldBay: String?,
        bayNo: String?,
        sku: String?,
        batchNo: String,
        qty: String
    )
}