package `in`.his.g2coutuk

import `in`.his.g2coutuk.dataModel.BayWithProductionItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bay_values.view.*
import java.util.*
import kotlin.collections.ArrayList

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.MyViewHolder>() {
    private var list: ArrayList<BayWithProductionItem> = ArrayList()
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        //if(!list.get(position).getSku().equalsIgnoreCase("empty")){
        holder.itemView.item_number_count.text = (position + 1).toString()
        holder.itemView.item_data_of_bay.text = list[position].bay
        holder.itemView.item_data_of_sku.text = list[position].sku
        holder.itemView.item_data_of_bay_barcode.text = list[position].barcode.toString()
        holder.itemView.item_data_of_product_barcode.text = list[position].pBarcode.toString()
        holder.itemView.item_data_of_qty.text = list[position].qty.toString()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bay_values, parent, false)
        return MyViewHolder(v)
    }

    fun setList(list: ArrayList<BayWithProductionItem>) {
        this.list = list
        notifyDataSetChanged()
    }

    fun sortFifo() {
        list.sortWith(Comparator { productionItem: BayWithProductionItem, t1: BayWithProductionItem ->
            Objects.requireNonNull<String>(
                productionItem.expiry
            ).compareTo(
                Objects.requireNonNull<String>(t1.expiry)
            )
        })
        setList(list)
    }

    fun sortLifo() {
        list.sortWith(Comparator { productionItem: BayWithProductionItem, t1: BayWithProductionItem ->
            Objects.requireNonNull<String>(
                t1.expiry
            ).compareTo(
                Objects.requireNonNull<String>(productionItem.expiry)
            )
        })
        setList(list)
    }

    /**
     * View holder class
     */
    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    companion object {
        private var instance: SearchAdapter? = null
        fun getInstance(): SearchAdapter {
            return if (instance == null) {
                instance = SearchAdapter()
                instance!!
            } else {
                instance!!
            }
        }
    }
}