package `in`.his.g2coutuk

import `in`.his.g2coutuk.LoginActivity.apiservice
import `in`.his.g2coutuk.apiinterface.ApiService
import `in`.his.g2coutuk.apiinterface.RetroClient
import `in`.his.g2coutuk.dataModel.*
import android.content.Context
import android.util.Log
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Utils {

    fun getSku(c: Context) {
        try {
            val call: Call<SkuListResponse> = apiservice.SKU_LIST_RESPONSE_CALL()
            call.enqueue(object : Callback<SkuListResponse?> {
                override fun onResponse(
                    call: Call<SkuListResponse?>,
                    response: Response<SkuListResponse?>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.skuData!!.isNotEmpty()) {
                            skusList.clear()
                            skuList = response.body()!!.skuData!!
                            skusList.clear()
                            for (i in skuList) {
                                skusList.add(i?.sku!!)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<SkuListResponse?>, t: Throwable) {
                    Toast.makeText(
                        c, "Please Check Network Connection First",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getBay() {
        try {
            val call = apiService.BAY_RESPONSE_CALL()
            call.enqueue(object : Callback<BayResponse> {
                override fun onFailure(call: Call<BayResponse>, t: Throwable) {

                }

                override fun onResponse(call: Call<BayResponse>, response: Response<BayResponse>) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.bay!!.isNotEmpty()) {
                            bayList = emptyList()
                            bayList = response.body()!!.bay!!
                            baysList.clear()
                            for (i in bayList) {

                                baysList.add(i!!.bay!!)
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getAllData() {
        try {
            val call = apiService.PRODUCTION_ALL_BARCODE_RESPONSE_CALL()
            call.enqueue(object : Callback<ResponseAllData> {
                override fun onFailure(call: Call<ResponseAllData>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<ResponseAllData>,
                    response: Response<ResponseAllData>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.bayWithProduction.isNullOrEmpty() -> {
                            }
                            else -> {
                                allData = emptyList()
                                allData = response.body()!!.bayWithProduction!!
                                Log.d("ListSize", allData.size.toString())
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getCommonList() {
        try {
            val call = apiService.COMMON_LIST_CALL()

            call.enqueue(object : Callback<CommonList> {
                override fun onFailure(call: Call<CommonList>, t: Throwable) {
                    callStatus.commonList(null)
                }

                override fun onResponse(call: Call<CommonList>, response: Response<CommonList>) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.commonData.isNullOrEmpty() -> {
                                callStatus.commonList(null)
                            }
                            else -> {
                                callStatus.commonList(response.body()!!.commonData)
                                Log.d(
                                    "commonListSize",
                                    response.body()!!.commonData!!.size.toString()
                                )
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getTransportDetails(orderId: String) {
        try {
            val call = apiService.GET_TRANSPORT_DATA_CALL(orderId)

            call.enqueue(object : Callback<GetTransportData> {
                override fun onFailure(call: Call<GetTransportData>, t: Throwable) {
                    print("cb")
                }

                override fun onResponse(
                    call: Call<GetTransportData>,
                    response: Response<GetTransportData>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.transportStatusData.isNullOrEmpty() -> {
                                callStatus.transportList(null)
                            }
                            else -> {
                                transportDetailsList = (response.body()!!.transportStatusData!!)
                                callStatus.transportList(transportDetailsList)
                                Log.d(
                                    "transportDetailListSize",
                                    transportDetailsList.size.toString()
                                )
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getLocationChangeData(orderId: String) {
        try {
            val call = apiService.LOCATION_CHANGE_DATA_CALL(orderId)

            call.enqueue(object : Callback<LocationChangeData> {
                override fun onFailure(call: Call<LocationChangeData>, t: Throwable) {
                    callStatus.changeOrder(null)
                }

                override fun onResponse(
                    call: Call<LocationChangeData>,
                    response: Response<LocationChangeData>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.locationChange.isNullOrEmpty() -> {
                                callStatus.changeOrder(null)
                            }
                            else -> {
                                callStatus.changeOrder(response.body()!!.locationChange)
                                Log.d(
                                    "locationChangeListSize",
                                    response.body()!!.locationChange!!.size.toString()
                                )
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun getOrderDetail(orderId: String) {
        try {
            val callback = apiService.GET_ORDER_DETAIL_CALL(orderId)
            callback.enqueue(object : Callback<GetOrderDetail> {
                override fun onFailure(call: Call<GetOrderDetail>, t: Throwable) {
                    callStatus.orderList(null)
                }

                override fun onResponse(
                    call: Call<GetOrderDetail>,
                    response: Response<GetOrderDetail>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.orderIdProduct.isNullOrEmpty() -> {
                                callStatus.orderList(null)
                            }
                            else -> {
                                callStatus.orderList(response.body()!!.orderIdProduct!!)
                                Log.d(
                                    "orderDetailListSize",
                                    response.body()!!.orderIdProduct!!.size.toString()
                                )
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun updateOrderStatus(id: String, status: String, c: Context) {
        try {
            val callback = apiService.UPDATE_COMMON_DATA_STATUS(id, status)

            callback.enqueue(object : Callback<LocationChangeResponse> {
                override fun onFailure(call: Call<LocationChangeResponse>, t: Throwable) {
                    if (status == "2") {
                        Toast.makeText(c, "Order Not Completed", Toast.LENGTH_SHORT).show()
                    }

                }

                override fun onResponse(
                    call: Call<LocationChangeResponse>,
                    response: Response<LocationChangeResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.message.isNullOrEmpty() -> {
                            }
                            else -> {
                                when (response.body()!!.message) {
                                    "Updated Successfully" -> {
                                        if (status == "2") {
                                            Toast.makeText(c, "Order Completed", Toast.LENGTH_SHORT)
                                                .show()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
        }
    }

    fun addCallStatus(call: CallStatus) {
        callStatus = call
    }

    fun insertOutGoods(insertOutGoods: InsertOutGoods, cQty: Int) {
        try {
            val call = apiService.INSERT_OUT_GOODS_CALL(insertOutGoods, cQty)

            call.enqueue(object : Callback<LocationChangeResponse> {
                override fun onFailure(call: Call<LocationChangeResponse>, t: Throwable) {
                    callStatus.itemUpdated(null)
                }

                override fun onResponse(
                    call: Call<LocationChangeResponse>,
                    response: Response<LocationChangeResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.message.isNullOrBlank() -> {
                                callStatus.itemUpdated(null)
                            }
                            else -> {
                                callStatus.itemUpdated(response.body()!!.message!!.toString())
                                Log.d(
                                    "insertOutGoods",
                                    response.body()!!.message!!.toString()
                                )
                            }
                        }
                    }

                }
            })
        } catch (e: Exception) {
        }
    }

    fun insertManualProduction(insertManualProduction: InsertManualProduction) {
        try {
            val call = apiService.INSERT_MANUAL_PRODUCTION(
                insertManualProduction
            )

            call.enqueue(object : Callback<ResponseInsertProduction> {
                override fun onResponse(
                    call: Call<ResponseInsertProduction>,
                    response: Response<ResponseInsertProduction>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {

                        }
                    }
                }

                override fun onFailure(call: Call<ResponseInsertProduction>, t: Throwable) {


                }
            })
        } catch (e: Exception) {
            print(e)
        }
    }

    fun insertWmsGoods(currentOrderId: Int?, insertModel: InsertModel) {
        Log.d("TAG", "insertWmsGoods: ${insertModel.qty}")
        try {
            val call = apiService.INSERT_GOODS_CALL(currentOrderId.toString(), insertModel)

            call.enqueue(object : Callback<LocationChangeResponse> {
                override fun onResponse(
                    call: Call<LocationChangeResponse>,
                    response: Response<LocationChangeResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.message == "Successful") {
                            Log.d("insertWmsGoods", "onResponse: ${response.body()!!.message}")
                            print(response.body()!!.message)
                        }
                    }
                }

                override fun onFailure(call: Call<LocationChangeResponse>, t: Throwable) {
                    print(t.message)
                }

            })
        } catch (e: Exception) {
        }
    }

    companion object {
        var allData: List<BayWithProductionItem?> = ArrayList<BayWithProductionItem>()
        var transportDetailsList: List<TransportStatusDataItem?> =
            ArrayList<TransportStatusDataItem>()
        lateinit var callStatus: CallStatus

        var skuList: List<SkuDataItem?> = ArrayList<SkuDataItem>()
        var bayList: List<BayItem?> = ArrayList<BayItem>()
        var skusList = ArrayList<String>()
        var baysList = ArrayList<String>()
        private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)

        var clickItem: OrderIdProductItem? = OrderIdProductItem()
    }
}

interface CallStatus {
    fun commonList(commonData: List<CommonDataItem?>?)

    fun orderList(orderIdProduct: List<OrderIdProductItem?>?)

    fun changeOrder(locationChange: List<LocationChangeItem?>?)

    fun itemUpdated(msg: String?)

    fun transportList(transportDetailsList: List<TransportStatusDataItem?>?)
}