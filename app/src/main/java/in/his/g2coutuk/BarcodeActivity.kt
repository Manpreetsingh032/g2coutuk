package `in`.his.g2coutuk

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.DialogFragment
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.barcode_scan.*
import kotlinx.android.synthetic.main.manual_barcode.*
import java.io.IOException


class BarcodeActivity : DialogFragment() {
    private lateinit var barcodeDetector: BarcodeDetector
    lateinit var c: Context
    lateinit var cameraSource: CameraSource
    var barcodeData = ""
    lateinit var surfaceView: SurfaceView
    private lateinit var manualBarcodeDialog: Dialog
    private val animation: Animation by lazy {
        AnimationUtils.loadAnimation(
            c,
            R.anim.line_anim
        )
    }
    lateinit var barcodeReceive: BarcodeReceive
    lateinit var bar: View
    private var cameraStatus = false
    var clickData: String? = ""

    override fun onAttach(context: Context) {
        c = context
        super.onAttach(context)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.barcode_scan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        barcodeReceive = activity as BarcodeReceive
//        clickData = intent.extras?.get("ScanBarcode")?.toString()
        surfaceView = view.findViewById(R.id.surface_view)
        bar = view.findViewById(R.id.bar)
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
                bar.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animation) {
                bar.visibility = View.GONE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })

        manualBarcodeDialog = Dialog(requireContext())
        with(manualBarcodeDialog) {
            setContentView(R.layout.manual_barcode)
            setCancelable(true)
            setCanceledOnTouchOutside(true)

            cancel_manual_barcode.setOnClickListener {
                dismiss()
            }

            submit_manual_barcode.setOnClickListener {
                if (manual_barcode.text.isNotEmpty()) {
                    barcodeData = manual_barcode.text.toString()
                    val intent = Intent(
                        c,
                        MainActivity::class.java
                    )
                    intent.putExtra("barcodeData", barcodeData)
                    intent.putExtra("ScanBarcode", clickData)

                    barcodeDetector.release()

//                    startActivity(intent)
                    cameraSource.stop()

                    dismiss()
                    barcodeReceive.onBarcode(barcodeData)
                    onBackPressed()
                    dismissAllowingStateLoss()
//                    finish()
                } else {
                    Toast.makeText(
                        c,
                        "Please enter barcode first",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            setOnCancelListener {
                onResume()
            }

            setOnDismissListener {
                onResume()
            }
        }


        close.setOnClickListener {
            bar.clearAnimation()
            onPause()
            cameraStatus = true
            manualBarcodeDialog.show()
        }

    }

    override fun onResume() {
        super.onResume()
        barcodeDetector = BarcodeDetector.Builder(c.applicationContext)
            .setBarcodeFormats(Barcode.ALL_FORMATS)
            .build()

        cameraSource = CameraSource.Builder(c.applicationContext, barcodeDetector)
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .setRequestedPreviewSize(1280, 1024)
            .setAutoFocusEnabled(true)
            .setRequestedFps(64.0f)
            .build()

        if (cameraStatus) {
            try {
                if (ActivityCompat.checkSelfPermission(
                        c.applicationContext,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.CAMERA),
                        1001
                    )
                    return
                }
                cameraSource.start(surfaceView.holder)

                bar.startAnimation(animation)

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }


        surfaceView.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                try {
                    if (ActivityCompat.checkSelfPermission(
                            c.applicationContext,
                            Manifest.permission.CAMERA
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            activity!!,
                            arrayOf(Manifest.permission.CAMERA),
                            1001
                        )
                        return
                    }
                    cameraSource.start(surfaceView.holder)

                    bar.startAnimation(animation)

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

            override fun surfaceChanged(
                holder: SurfaceHolder,
                format: Int,
                width: Int,
                height: Int
            ) {
                print("a")
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                try {
                    cameraSource.release()
                } catch (e: Exception) {
                }
                bar.clearAnimation()
            }
        })

        startScan()
    }

    private fun startScan() {
        try {
            if (!barcodeDetector.isOperational) {
                Log.d("TAG", "Detector dependencies not loaded yet")
            } else {
                barcodeDetector.setProcessor(object : Detector.Processor<Barcode> {
                    override fun release() {
                        bar.clearAnimation()
                    }

                    override fun receiveDetections(detections: Detector.Detections<Barcode>) {
                        val barCodes = detections.detectedItems
                        if (barCodes.size() != 0) {
                            Log.d("TAG", "receiveDetections: ${barCodes.valueAt(0).displayValue}")
                            try {
                                activity?.runOnUiThread {
                                    Toast.makeText(
                                        c,
                                        barCodes.valueAt(0).displayValue, Toast.LENGTH_SHORT
                                    ).show()
                                }

                            } catch (e: Exception) {
                            }
                            barcodeData = barCodes.valueAt(0).displayValue

                            /*val intent = Intent(
                                c,
                                MainActivity::class.java
                            )
                            intent.putExtra("barcodeData", barcodeData)
                            intent.putExtra("ScanBarcode", clickData)*/
                            barcodeDetector.release()
//                            startActivity(intent)
                            /* try {
                                 activity?.runOnUiThread {
                                     cameraSource.stop()
                                 }
                             } catch (e: Exception) {

                             }*/
                            activity?.runOnUiThread {
                                cameraSource.stop()
                                dismiss()
                                barcodeReceive.onBarcode(barcodeData)
//                                dialog?.dismiss()
                            }



//                            finish()
                        }
                    }
                })
            }
        } catch (e: Exception) {
            Log.d("TAG", "$e")
        }

    }


    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode != 1001) {
            Log.d("TAG", "Got unexpected permission result: $requestCode")
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                cameraSource.start(surfaceView.holder)
                bar.startAnimation(animation)
            } catch (e: Exception) {
                Log.d("CameraTAG", e.toString())
            }
        }
    }

    override fun onPause() {
        super.onPause()
        try {
            cameraSource.stop()
        } catch (e: Exception) {
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
//        Toast.makeText(c, "$barcodeData", Toast.LENGTH_SHORT).show()
        super.onDismiss(dialog)
    }

}

interface BarcodeReceive {
    fun onBarcode(barcode: String)
}