package `in`.his.g2coutuk

import `in`.his.g2coutuk.dataModel.OrderIdProductItem
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bay_values.view.*
import java.util.*

class FirstOrderListAdapter : RecyclerView.Adapter<FirstOrderListAdapter.MyViewHolder>() {
    private var list: ArrayList<OrderIdProductItem?> = ArrayList()
    private lateinit var setItemClick: SetFirstItemClick
    private var holdStatus = true
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        //if(!list.get(position).getSku().equalsIgnoreCase("empty")){
        holder.itemView.item_number_count.text = (position + 1).toString()
        holder.itemView.item_data_of_bay.text = list[position]?.bay
        holder.itemView.item_data_of_sku.text = list[position]?.sku
        holder.itemView.item_data_of_bay_barcode.text = list[position]?.barcode.toString()
        holder.itemView.item_data_of_product_barcode.text = list[position]?.pBarcode.toString()
        holder.itemView.item_data_of_qty.text = list[position]?.qty.toString()

        holder.itemView.setOnClickListener {
            if (!holdStatus) {
                setItemClick.onClick(
                    list[position]?.barcode,
                    list[position]?.sku,
                    list[position]?.batchNo,
                    list[position]?.qty,
                    list[position]?.pBarcode,
                    list[position]?.expiry,
                    list[position]?.orderId
                )
                Utils.clickItem = list[position]
            }

        }

        if (holder.itemView.item_data_of_qty.text.toString() == "0") {
            holder.itemView.setBackgroundResource(R.color.green)
        } else {
            holder.itemView.setBackgroundResource(R.color.red)
        }

        if (holdStatus) {
            holder.itemView.setBackgroundResource(R.color.yellow)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bay_values, parent, false)
        return MyViewHolder(v)
    }

    fun setList(
        list: ArrayList<OrderIdProductItem?>,
        firstHoldStatus: Boolean
    ) {
        this.list = list
        holdStatus = firstHoldStatus
        notifyDataSetChanged()
    }

    fun addItemClick(setItemClick: SetFirstItemClick) {
        this.setItemClick = setItemClick
    }

    /**
     * View holder class
     */
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    companion object {
        private var instance: FirstOrderListAdapter? = null
        fun getInstance(): FirstOrderListAdapter {
            return if (instance == null) {
                instance = FirstOrderListAdapter()
                instance!!
            } else {
                instance!!
            }
        }
    }
}

interface SetFirstItemClick {
    fun onClick(
        barcode: String?,
        sku: String?,
        batchNo: String?,
        qty: Int?,
        pBarcode: String?,
        expiry: String?,
        orderId: Int?
    )
}
