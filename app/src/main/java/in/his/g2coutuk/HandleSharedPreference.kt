package `in`.his.g2coutuk

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("CommitPrefEdits")
class HandleSharedPreference(private val c: Context) {

    init {
        sharedPreferences =
            c.applicationContext?.getSharedPreferences(preferences, Context.MODE_PRIVATE)
    }


    fun insertSku(skuList: Set<String>) {
        editor = sharedPreferences?.edit()!!
        editor.remove(skuPreference)
        editor.putStringSet(skuPreference, skuList)
        editor.apply()
        editor.commit()
    }

    fun skuList() = run {
        sharedPreferences?.getStringSet(skuPreference, emptySet())
    }

    fun insertLineNos(lineList: Set<String>) {
        editor = sharedPreferences?.edit()!!
        editor.remove(linePreference)
        editor.putStringSet(linePreference, lineList)
        editor.apply()
        editor.commit()
    }

    fun lineList() = run {
        sharedPreferences?.getStringSet(linePreference, emptySet())
    }

    fun insertFirstOrderNo(firstOrderId: String) {
        editor = sharedPreferences?.edit()!!
        editor.putString(firstOrderPreference, firstOrderId)
        editor.apply()
        editor.commit()
    }

    fun firstOrderNoPref() = kotlin.run {
        sharedPreferences?.getString(firstOrderPreference, "")
    }

    fun insertSecondOrderNo(secondOrderId: String) {
        editor = sharedPreferences?.edit()!!
        editor.putString(secondOrderPreference, secondOrderId)
        editor.apply()
        editor.commit()
    }

    fun secondOrderNoPref() = kotlin.run {
        sharedPreferences?.getString(secondOrderPreference, "")
    }


    companion object {
        private const val preferences = "Preference"
        private const val skuPreference = "skuPreference"
        private const val linePreference = "LinePreference"
        private const val firstOrderPreference = "FirstOrderPreference"
        private const val secondOrderPreference = "SecondOrderPreference"
        private lateinit var editor: SharedPreferences.Editor
        var sharedPreferences: SharedPreferences? = null
    }
}